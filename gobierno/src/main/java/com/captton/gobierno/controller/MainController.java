package com.captton.gobierno.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.gobierno.model.Banco;
import com.captton.gobierno.model.Empleado;
import com.captton.gobierno.model.Empresa;
import com.captton.gobierno.model.InputEmpleado;
import com.captton.gobierno.model.InputEmpresa;
import com.captton.gobierno.repository.DaoBanco;
import com.captton.gobierno.repository.DaoEmpleado;
import com.captton.gobierno.repository.DaoEmpresa;


@RestController
@RequestMapping({"/gob"})
public class MainController {

	@Autowired
	private DaoBanco daobanco;
	private DaoEmpresa daoempresa;
	private DaoEmpleado daoempleado;
	
	
//APIS
	
	@PostMapping(path = {"/banco"})//crear banco
	public Banco create(@RequestBody Banco bank) {
		return daobanco.save(bank);
	}
	
	@PostMapping(path = {"/empresa"})//crear empresa
	public ResponseEntity<Object> Empresa(@RequestBody InputEmpresa emp) {
		Banco bank = daobanco.findById(emp.getIdBanco()).orElse(null);
		JSONObject obj = new JSONObject();

		if (bank != null) {//banco existe agrego empresa
			Empresa auxEmp = new Empresa();
			auxEmp.setBanco(bank);
			auxEmp.setDescrip(emp.getEmp().getDescrip());
			auxEmp.setDireccion(emp.getEmp().getDireccion());
			auxEmp.setLocalidad(emp.getEmp().getLocalidad());
			auxEmp.setNombre(emp.getEmp().getNombre());
			Empresa emppost = daoempresa.save(auxEmp);
			try {
				obj.put("error", 0);
				obj.put("results", emppost.getId());
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			// Sino exite, devuelvo error
			try {
				obj.put("error", 1);
				obj.put("message", "No existe el banco ingresado");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return ResponseEntity.ok().body(obj.toString());
	}
	
	@PostMapping(path = { "/empleado" })
	public ResponseEntity<Object> altaEmpleade(@RequestBody InputEmpleado emplo) {//agrega empleados

		JSONObject obj = new JSONObject();
		Empresa empresa = daoempresa.findById(emplo.getIdEmpresa()).orElse(null);

		if (empresa != null) {
			
			Empleado empaux = new Empleado();
			empaux.setEmpresa(empresa);
			empaux.setNombre(emplo.getEmp().getNombre());
			empaux.setApellido(emplo.getEmp().getApellido());
			empaux.setDireccion(emplo.getEmp().getDireccion());
			empaux.setDoc(emplo.getEmp().getDoc());
			Empleado emplead = daoempleado.save(empaux);
			
			try {
				obj.put("error", 0);
				obj.put("results", emplead.getId());
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			// Si no existe, devuelvo error
			try {
				obj.put("error", 1);
				obj.put("message", "No se encontro la empresa");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return ResponseEntity.ok().body(obj.toString());

	}
	
	@PutMapping(path = { "/newdirec/{dni}/{direc}" })//cambiar direc pasando dni
	public ResponseEntity<Object> cambiarDireccionPorDni(@PathVariable("doc") String doc,
			@PathVariable("direc") String direc) { //busca emp x doc y cambia direc
		List<Empleado> empFound = daoempleado.findBydoc(doc);
		JSONObject obj = new JSONObject();
		if (empFound.isEmpty()) {//no hay emp c/ ese doc error
			try {
				obj.put("error", 1);
				obj.put("Message", "No hay empleados con el documento ingresado");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			for (Empleado emple : empFound) {
	
				JSONObject aux = new JSONObject();
				emple.setDireccion(direc);
				daoempleado.save(emple);
				try {
					aux.put("direccion", emple.getDireccion());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				obj.put("error", 0);
				obj.put("Message", "Cambios realizados");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return ResponseEntity.ok().body(obj.toString());
	}

	@GetMapping(path = { "/listemp/{id_emp}" })
	public ResponseEntity<Object> listarEmpleadosdeEmpresa(@PathVariable Long id_empresa) {
		// Lista todos los empleados de una empresa
		Empresa auxEmp = daoempresa.findById(id_empresa).orElse(null);
		List<Empleado> empleadosFound = daoempleado.findByempresa(auxEmp);
		JSONObject obj = new JSONObject();

		if (auxEmp == null) {
			// Si no existe la empresa
			try {
				obj.put("error", 1);
				obj.put("Message", "Empresa no existente");
			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			if (empleadosFound.isEmpty()) { //if empresa no tiene empleados
				try {
					obj.put("error", 1);
					obj.put("Message", "Empresa sin empleados.");
				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else {//empresa c/ empleados
				
				JSONArray empArray = new JSONArray();
					for (Empleado auxemp : empleadosFound) {
					// recorro la lista de empleados, cargando JsonArray con Json de cada empleado
					JSONObject aux = new JSONObject();
						try {
						aux.put("nombre", auxemp.getNombre());
						aux.put("direccion", auxemp.getDireccion());
						aux.put("apellido", auxemp.getApellido());
						aux.put("dni", auxemp.getDoc());
						} catch (JSONException e) {
						e.printStackTrace();
						}
					empArray.put(aux);
					}
				try {
					// JSON LISTA
					obj.put("error", 0);
					obj.put("empleados", empArray);
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

		}
		return ResponseEntity.ok().body(obj.toString());
	}

}