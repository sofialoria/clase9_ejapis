package com.captton.gobierno.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.gobierno.model.Banco;

public interface DaoBanco extends JpaRepository<Banco, Long> {

}
