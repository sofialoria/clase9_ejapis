package com.captton.gobierno.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "banco")
public class Banco {

	@Id
	@GeneratedValue
	private Long id;
	
	private String nombre;
	private String direc;
	private String localidad;
	
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="banco" )
	  private List<Empresa> listaEmpresas;
	

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDirec() {
		return direc;
	}
	public void setDirec(String direc) {
		this.direc = direc;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public List<Empresa> getListaEmpresas() {
		return listaEmpresas;
	}
	public void setListaEmpresas(List<Empresa> listaEmpresas) {
		this.listaEmpresas = listaEmpresas;
	}
	
	
}
